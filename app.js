const PORT = 8080;


const http = require('http');
//const log = require('log');


const server = http.createServer((request, response) => {
    const { headers } = request;

    for (let k in headers) {
        console.log(k + ' --> ' + headers[k]);
    }

    console.log('method: ' + request.method);

    let body = [];
    request.on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        console.log('body: ' + body);

        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write('<html><body>cheers!</body></html>');
        response.end();
    });
});

server.listen(PORT);